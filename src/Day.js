import React from "react";

const Day = ({month}) => {
    const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    const day = new Date(`${month} 1 , 2021`)
    return (
        <div>
            Month {month}/2021 starts {weekday[day.getDay()]}
        </div>
    )
}

export default Day;
