import React from "react";
import Day from "./Day";
import { useState } from "react";

const Month = () => {
    const [mth, setMth] = useState(1);
    const months = [1,2,3,4,5,6,7,8,9,10,11,12];
    const monthList = months.map(month => {
        return (
        <option value={month}>{month}</option>)
    });
    return (
        <div>
            <select onChange={e => setMth(e.target.value)}>{monthList}</select>
            <Day month={mth} />
        </div>
    )
}

export default Month;
